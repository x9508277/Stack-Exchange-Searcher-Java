package com.example.dev.StackExchangeSearcher;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StackExchangeSearcherApplicationTests {

	@Test
	public void constructUri() {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme("http").host("api.stackexchange.com").path("/2.2/search")
				.query("page={page}")
				.query("pagesize={pageSize}")
				.query("order={order}")
				.query("sort={sort}")
				.query("intitle={intitle}")
				.query("site={site}")
				.query("filter=!9Z(-wwK4f").buildAndExpand("1", "25", "desc", "activity", "c#", "stackoverflow").encode();

		assertEquals("http://api.stackexchange.com/2.2/search?page=1&pagesize=25&order=desc&sort=activity&intitle=c%23&site=stackoverflow&filter=!9Z(-wwK4f", uriComponents.toUriString());
	}

}

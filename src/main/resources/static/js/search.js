function changePage(location, currentPage, page) {
    window.location.href = location.replace("page="+currentPage, "page="+ page);
}

$(document).ready(function () {
    var current_page_num = $('#current_page_num');
    var currentPage = parseInt(current_page_num.val());
    var location = window.location.href;

    $('#prev_btn').click(function () {
        changePage(location, currentPage, currentPage - 1);
    });

    $('#next_btn').click(function () {
        changePage(location, currentPage, currentPage + 1);
    });

    $('#page_form').submit(function (e) {
        var value = parseInt(current_page_num.val());
        if (value <= 0){
            return;
        }
        e.preventDefault();
        changePage(location, currentPage, value);

    })
})
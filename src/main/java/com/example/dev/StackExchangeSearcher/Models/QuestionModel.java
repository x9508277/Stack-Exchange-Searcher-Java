package com.example.dev.StackExchangeSearcher.Models;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QuestionModel {
    private ArrayList<String> tags;
    private OwnerModel owner;
    private boolean is_answered;
    private int view_count;
    private int answer_count;
    private int score;
    private Date last_activity_date;
    private Date creation_date;
    private int question_id;
    private String body_markdown;
    private String link;
    private String title;

    public static QuestionModel fromJson(JSONObject json) throws JSONException {
        QuestionModel question = new QuestionModel();
        question.setAnswer_count(json.getInt("answer_count"));
        question.setBody_markdown(json.getString("body_markdown"));
        question.setCreation_date(json.getInt("creation_date"));
        question.setIs_answered(json.getBoolean("is_answered"));
        question.setLast_activity_date(json.getInt("last_activity_date"));
        question.setLink(json.getString("link"));
        question.setOwner(OwnerModel.fromJson(json.getJSONObject("owner")));
        question.setQuestion_id(json.getInt("question_id"));
        question.setScore(json.getInt("score"));
        ArrayList<String> tagsArray = new ArrayList<>();
        JSONArray jsonTags = json.getJSONArray("tags");
        if (jsonTags != null) {
            int length = jsonTags.length();
            for (int i = 0; i < length; i++)
                tagsArray.add(jsonTags.get(i).toString());
        }
        question.setTags(tagsArray);
        question.setTitle(json.getString("title"));
        question.setView_count(json.getInt("view_count"));
        return question;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public OwnerModel getOwner() {
        return owner;
    }

    public void setOwner(OwnerModel owner) {
        this.owner = owner;
    }

    public boolean isIs_answered() {
        return is_answered;
    }

    public void setIs_answered(boolean is_answered) {
        this.is_answered = is_answered;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }

    public int getAnswer_count() {
        return answer_count;
    }

    public void setAnswer_count(int answer_count) {
        this.answer_count = answer_count;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Date getLast_activity_date() {
        return last_activity_date;
    }

    public void setLast_activity_date(int last_activity_date) {
        this.last_activity_date = new Date(last_activity_date * 1000L);
    }

    public String getCreation_date() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy 'at' HH:mm");
        return dateFormat.format(creation_date);
    }

    public void setCreation_date(int creation_date) {
        this.creation_date = new Date(creation_date *  1000L);
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public String getBody_markdown() {
        return body_markdown.length() > 200 ? body_markdown.substring(0, 200) + "..." : body_markdown;
    }

    public void setBody_markdown(String body_markdown) {
        this.body_markdown = body_markdown;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

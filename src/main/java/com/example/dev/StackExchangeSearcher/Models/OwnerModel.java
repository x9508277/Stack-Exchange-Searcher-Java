package com.example.dev.StackExchangeSearcher.Models;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class OwnerModel {
    private int reputation;
    private int user_id;
    private String user_type;
    private String profile_image;
    private String display_name;
    private String link;
    private boolean isExist;

    static OwnerModel fromJson(JSONObject json) throws JSONException {
        OwnerModel owner = new OwnerModel();
        owner.setDisplay_name(json.getString("display_name"));
        owner.setUser_type(json.getString("user_type"));
        if (Objects.equals(owner.getUser_type(), "does_not_exist"))
            return owner;
        owner.setExist(true);
        owner.setLink(json.getString("link"));
        owner.setProfile_image(json.getString("profile_image"));
        owner.setReputation(json.getInt("reputation"));
        owner.setUser_id(json.getInt("user_id"));

        return owner;
    }

    public int getReputation() {
        return reputation;
    }

    private void setReputation(int reputation) {
        this.reputation = reputation;
    }

    public int getUser_id() {
        return user_id;
    }

    private void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    private String getUser_type() {
        return user_type;
    }

    private void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getProfile_image() {
        return profile_image;
    }

    private void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getDisplay_name() {
        return display_name;
    }

    private void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getLink() {
        return link;
    }

    private void setLink(String link) {
        this.link = link;
    }

    public boolean isExist() {
        return isExist;
    }

    public void setExist(boolean exist) {
        isExist = exist;
    }
}

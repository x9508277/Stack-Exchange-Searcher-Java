package com.example.dev.StackExchangeSearcher.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class SiteModel {
    private String api_site_parameter;
    private String name;
    private String icon_url;

    public static SiteModel fromJson(JSONObject json) throws JSONException {
        SiteModel model = new SiteModel();
        model.setApi_site_parameter(json.getString("api_site_parameter"));
        model.setName(json.getString("name"));
        model.setIcon_url(json.getString("icon_url"));
        return model;
    }

    public String getApi_site_parameter() {
        return api_site_parameter;
    }

    public void setApi_site_parameter(String api_site_parametr) {
        this.api_site_parameter = api_site_parametr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }
}

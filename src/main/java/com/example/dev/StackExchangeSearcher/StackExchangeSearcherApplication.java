package com.example.dev.StackExchangeSearcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.CharacterEncodingFilter;

@SpringBootApplication
public class StackExchangeSearcherApplication {

    public static void main(String[] args) {
        SpringApplication.run(StackExchangeSearcherApplication.class, args);
    }
}

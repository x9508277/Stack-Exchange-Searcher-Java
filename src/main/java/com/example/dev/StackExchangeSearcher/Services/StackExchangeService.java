package com.example.dev.StackExchangeSearcher.Services;

import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.zip.GZIPInputStream;

@Service
public class StackExchangeService implements IStackExchangeService {
    private static final String SCHEME = "http";
    private static final String HOST = "api.stackexchange.com";
    private static final String SEARCH_PATH = "/2.2/search";
    private static final String SEARCH_FILTER = "filter=!9Z(-wwK4f";
    private static final String SITES_PATH = "/2.2/sites";
    private static final String SITES_FILTER = "filter=!*L1)ih4ub*LWS)TP";
    private static final String PAGE_QUERY = "page={page}";
    private static final String PAGESIZE_QUERY = "pagesize={pageSize}";
    private static final String ORDER_QUERY = "order={order}";
    private static final String SORT_QUERY = "sort={sort}";
    private static final String INTITLE_QUERY = "intitle={intitle}";
    private static final String SITE_QUERY = "site={site}";

    @Override
    public String getSearchResult(String intitle, String site, String page, String pageSize, String order, String sort) throws ExecutionException, InterruptedException {
        return getRequestWithGzip(UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(SEARCH_PATH)
                .query(PAGE_QUERY)
                .query(PAGESIZE_QUERY)
                .query(ORDER_QUERY)
                .query(SORT_QUERY)
                .query(INTITLE_QUERY)
                .query(SITE_QUERY)
                .query(SEARCH_FILTER)
                .buildAndExpand(page, pageSize, order, sort, intitle, site)
                .encode(StandardCharsets.UTF_8)).get();
    }

    @Override
    public String getSites() throws ExecutionException, InterruptedException {
        return getRequestWithGzip(UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(SITES_PATH)
                .query(SITES_FILTER)
                .build()
                .encode(StandardCharsets.UTF_8)).get();
    }

    @Async
    @Override
    public Future<String> getRequestWithGzip(UriComponents uriComponents) {
        RestTemplate template = new RestTemplate();
        ResponseEntity<byte[]> result = template.exchange(new RequestEntity<>(HttpMethod.GET, uriComponents.toUri()), byte[].class);

        try {
            byte[] body = result.getBody();

            if (body == null) return null;

            ByteArrayInputStream bis = new ByteArrayInputStream(result.getBody());
            GZIPInputStream gis = new GZIPInputStream(bis);
            BufferedReader br = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            gis.close();
            bis.close();
            return AsyncResult.forValue(sb.toString());
        } catch (IOException e) {
            return AsyncResult.forValue(e.getMessage());
        }
    }


}

package com.example.dev.StackExchangeSearcher.Services;

import org.springframework.web.util.UriComponents;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public interface IStackExchangeService {
    Future<String> getRequestWithGzip(UriComponents uriComponents);
    String getSites() throws ExecutionException, InterruptedException;
    String getSearchResult(String intitle, String site, String page, String pageSize, String order, String sort) throws ExecutionException, InterruptedException;
}

package com.example.dev.StackExchangeSearcher.Controllers;

import com.example.dev.StackExchangeSearcher.Models.QuestionModel;
import com.example.dev.StackExchangeSearcher.Models.SiteModel;
import com.example.dev.StackExchangeSearcher.Services.IStackExchangeService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
public class SearchController {
    private final IStackExchangeService stackExchangeService;
    private ArrayList<QuestionModel> questions = new ArrayList<>();
    private ArrayList<SiteModel> sites = new ArrayList<>();

    @Autowired
    public SearchController(IStackExchangeService stackExchangeService) {
        this.stackExchangeService = stackExchangeService;
    }

    @RequestMapping(value = {"/search", "/search/"}, method = RequestMethod.GET)
    public ModelAndView search(@RequestParam Map<String, String> params) {
        Map<String, Object> model = new HashMap<>();
        model.put("title", "StackExchange Searcher");
        questions.clear();
        sites.clear();

        String intitle = params.get("search_text");
        String site = params.get("site");
        String page = params.get("page");
        String pageSize = params.get("pagesize");
        String order = params.get("order");
        String sort = params.get("sort");

        boolean isFirstPage = page != null && page.equals("1");
        model.put("isFirstPage", isFirstPage);

        model.put("search_text", intitle != null && !intitle.equals("") ? intitle : "");

        String searchResult = null, sitesResult = null;
        try {
            searchResult = stackExchangeService.getSearchResult(
                    intitle,
                    site == null ? "stackoverflow" : site,
                    page == null ? "1" : page,
                    pageSize == null ? "25" : pageSize,
                    order == null ? "desc" : order,
                    sort == null ? "activity" : sort);
            sitesResult = stackExchangeService.getSites();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        if (searchResult == null && sitesResult == null) return null;

        try {
            JSONObject json = new JSONObject(searchResult);
            boolean isLastPage = !json.getBoolean("has_more");
            model.put("isLastPage", isLastPage);

            JSONArray items = json.getJSONArray("items");
            int length = items.length();
            boolean isResultsExist = length > 0;
            model.put("isResultsExist", isResultsExist);
            model.put("currentPage", isFirstPage ? "1" : page);


            for (int i = 0; i < length; i++) {
                questions.add(QuestionModel.fromJson(items.getJSONObject(i)));
            }
            model.put("questions", questions);

            json = new JSONObject(sitesResult);
            items = json.getJSONArray("items");
            length = items.length();

            for (int i = 0; i < length; i++)
                sites.add(SiteModel.fromJson(items.getJSONObject(i)));

            model.put("sites", sites);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new ModelAndView("search", model);
    }
}

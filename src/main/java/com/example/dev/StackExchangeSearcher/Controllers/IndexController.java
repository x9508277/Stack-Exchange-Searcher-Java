package com.example.dev.StackExchangeSearcher.Controllers;

import com.example.dev.StackExchangeSearcher.Models.QuestionModel;
import com.example.dev.StackExchangeSearcher.Services.IStackExchangeService;
import com.example.dev.StackExchangeSearcher.Services.StackExchangeService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.omg.CORBA.NameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.zip.GZIPInputStream;

@RestController
public class IndexController {

    @GetMapping("/")
    public ModelAndView index(){
        Map<String, Object> model = new HashMap<>();
        model.put("title", "StackExchange Searcher");
        return new ModelAndView("index", model);
    }
}
